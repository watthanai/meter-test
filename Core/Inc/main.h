/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef enum eMode {POST, IDLE, SETTING, RUNNING, WAITING, ALARM, FAILSAFE} Mode;
typedef enum eEvent {EV_NONE, EV_OUTPUT_ERR, EV_COMMU_ERR, EV_SENSOR_ERR} Event;
typedef struct {
	Mode output;
	Mode commu;
	Mode sensor;
    Mode tracking;
} evState_t;
typedef struct user_data_t{
	uint16_t ID_value;
	uint16_t Power_value;
	uint16_t Amp_value;
	uint16_t Volt_value;

}user_data_t;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern volatile Mode currentMode;
extern int errno;
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RCC_OSC_IN_Pin GPIO_PIN_0
#define RCC_OSC_IN_GPIO_Port GPIOH
#define DBG_TX_Pin GPIO_PIN_2
#define DBG_TX_GPIO_Port GPIOA
#define DBG_RX_Pin GPIO_PIN_3
#define DBG_RX_GPIO_Port GPIOA
#define EXT_SCK_Pin GPIO_PIN_5
#define EXT_SCK_GPIO_Port GPIOA
#define EXT_MISO_Pin GPIO_PIN_6
#define EXT_MISO_GPIO_Port GPIOA
#define EXT_MOSI_Pin GPIO_PIN_7
#define EXT_MOSI_GPIO_Port GPIOA
#define LD1_Pin GPIO_PIN_0
#define LD1_GPIO_Port GPIOB
#define EXT_TCXO_Pin GPIO_PIN_13
#define EXT_TCXO_GPIO_Port GPIOF
#define EXT_BUSY_Pin GPIO_PIN_14
#define EXT_BUSY_GPIO_Port GPIOF
#define EXT_SW_Pin GPIO_PIN_15
#define EXT_SW_GPIO_Port GPIOF
#define EXT_DIO2_Pin GPIO_PIN_9
#define EXT_DIO2_GPIO_Port GPIOE
#define EXT_DIO1_Pin GPIO_PIN_11
#define EXT_DIO1_GPIO_Port GPIOE
#define LD3_Pin GPIO_PIN_14
#define LD3_GPIO_Port GPIOB
#define EXT_SS_Pin GPIO_PIN_14
#define EXT_SS_GPIO_Port GPIOD
#define MB1_TX_Pin GPIO_PIN_10
#define MB1_TX_GPIO_Port GPIOC
#define MB1_RX_Pin GPIO_PIN_11
#define MB1_RX_GPIO_Port GPIOC
#define RS485_RX_Pin GPIO_PIN_9
#define RS485_RX_GPIO_Port GPIOG
#define RS485_TX_Pin GPIO_PIN_14
#define RS485_TX_GPIO_Port GPIOG
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
